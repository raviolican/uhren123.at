

<?php
/*
 * Using modified layout for optimizing conversions!
 */
?>
@extends('layouts.app')

@section('content')
    <script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "NewsArticle",
  "mainEntityOfPage": {
    "@type": "WebPage",
    "@id": "{{Request::url() }}"
  },
  "headline": "{{$article->title}}",
  "image": {
    "@type": "ImageObject",
    "url": "{{url($article->article_image)}}",
    "height": 800,
    "width": 800
  },
  "datePublished": "{{$article->created_at}}",
  "dateModified": "{{$article->updated_at}}",
  "author": {
    "@type": "Person",
    "name": "Simon Daum"
  },
   "publisher": {
    "@type": "Organization",
    "name": "{{$settings["title"]}}",
    "logo": {
      "@type": "ImageObject",
      "url": "{{asset("https://media.giphy.com/media/3og0ITZD4v7Li9LX44/200w_d.gif")}}",
      "width": 200,
      "height": 50
    }
  },
  "description": "{{$article->description}}"
}
</script>

    <div class="container">

        <ol class="breadcrumb" style="" itemscope itemtype="http://schema.org/BreadcrumbList">
            <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                <a href="{{url("/")}}" itemscope itemtype="http://schema.org/Thing" itemprop="item">
                    <span itemprop="name">Home</span>
                </a>
                <meta itemprop="position" content="1" />
            </li>

            <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                <a href="{{url("/blog")}}" itemscope itemtype="http://schema.org/Thing" itemprop="item" >
                    <span itemprop="name">Blog</span>
                </a>
                <meta itemprop="position" content="2" />
            </li>

            <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                <a href="{{Request::url() }}" itemscope itemtype="http://schema.org/Thing" itemprop="item">
                    <span itemprop="name">{{$article->title}}</span>
                </a>
                <meta itemprop="position" content="3" />
            </li>
        </ol>



        <div class="row">
            <div class="col-md-9">
                <img src="{{url($article->article_image)}}" alt="">
                <h1  >{{$article->title}}</h1>
                <div class="blog-post-tags">
                    <ul class="list-unstyled list-inline blog-info">
                        <li><i class="fa fa-calendar"></i> {{$article->created_at->diffforhumans()}}</li>
                        <li><i class="fa fa-pencil"></i> Uhren123 Team</li>
                    </ul>
                </div>

                <!--Blog Post-->
                <div class="blog margin-bottom-40">
                    {!!$article->text !!}


                </div>
                <!--End Blog Post-->

            </div>
            <!-- Right Sidebar -->
            <div class="col-md-3">
                <!-- Social Icons -->
                <div class="magazine-sb-social margin-bottom-30">
                    <div class="headline headline-md"><h2>Folgen</h2></div>
                    <!-- Go to www.addthis.com/dashboard to customize your tools -->
                    <div class="addthis_inline_follow_toolbox"></div>
                    <div class="clearfix"></div>
                </div>
                <!-- End Social Icons -->


                <!-- End Posts -->

                <!-- Tabs Widget -->
                <div class="tab-v2 margin-bottom-40">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#home-1">About Us</a></li>

                    </ul>
                    <div class="tab-content">
                        <div id="home-1" class="tab-pane active">
                            <p>
                                Uhren123 ist spezialisert auf Uhren, wir bieten günstige Preise mit bester Qualität bei schneller Lieferung
                            </p>
                        </div>

                    </div>
                </div>
                <!-- End Tabs Widget -->


            </div>
            <!-- End Right Sidebar -->

        </div>
    </div>
@endsection
@section('footer')
    <script>
        $(document).ready(function () {
            ajaxuse = false;


            $(".qtyChekc").on("change", function () {

                if(ajaxuse == true)
                {
                    return;
                }
                else
                {
                    ajaxuse = true;
                    $.ajax({
                        url: '{{url('cart/add/')}}' + "/" + $(this).attr("asin") + "/" + $(this).val(),
                        type: 'POST',
                        async: false,
                        dataType: "",
                        data: {
                            '_token': '{{csrf_token()}}'
                        },
                        success: function (result) {
                            ajaxuse = false;
                            location.reload();
                        }
                    });
                    ajaxuse = false;
                }
            });

            $(".close").on("click", function () {
                if(ajaxuse == true)
                {
                    return;
                }
                else
                {
                    ajaxuse = true;
                    $.ajax({
                        url: '{{url('cart/delete/')}}' + "/" + $(this).attr("asin"),
                        type: 'POST',
                        async: false,
                        dataType: "",
                        data: {
                            '_token': '{{csrf_token()}}'
                        },
                        success: function (result) {
                            ajaxuse = false;
                            location.reload();
                        }
                    });
                    ajaxuse = false;

                }

            });
        });

    </script>
@endsection