<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Cart">
    <meta name="author" content="">
    <link rel="icon" href="">
    <title>Cart</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Custom styles for this template -->
    <link href="{{asset('css/main.css')}}" rel="stylesheet">
    <link href="{{asset('css/bootflat.min.css')}}" rel="stylesheet">
    <!-- Latest compiled and minified JavaScript -->
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.3/js/bootstrap-select.min.js"></script>
    @yield('header')
    {!! $settings["additional_js"] !!}
</head>
<body>
<!-- Fixed navbar -->
<div class="top-hdr" style="height: 50px; width: 100%">
    <div style="text-align: center;">
        <img src="{{asset("https://media.giphy.com/media/3og0ITZD4v7Li9LX44/200w_d.gif")}}" alt="">
        <ul class="nav navbar-nav" style="float: right">
            <li style="float: right" >
                <a href="{{url("admin")}}" title="Admin" class="dropdown-toggle">
                    <b style="color: gray">
                        Admin
                    </b>
                </a>
            </li>
        </ul>
    </div>
</div>

<div class="connn">
    @yield('content')

    @include("layouts.footer")
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    @yield('footer')
</div>

</body>
</html>
<?php /* PLEASE NOT DELETE THIS */
echo  "<!--
           Name: AffiliateSHOP <Amazon>
        Version: 1.4.0a
         Author: Simon Daum daumsimon94@gmail.com
       Purchase: LINK
    Description: This is an Amazon Affiliate shop system. A powerful tool to manage affiliate products from Amazon.
                 THIS IS NOT FREE SOFTWARE
-->" /*PLEASE NOT DELETE THIS */;?>