@extends('layouts.app')

@section('header')
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

    <link href="{{asset("css/nouislider.min.css")}}" rel="stylesheet">
@endsection
@section('content')
    <script src="nouislider.min.js"></script>
    <script src="{{asset("js/nouislider.min.js")}}"></script>



    <div class="row">
        <div class="col-md-12">
            <span >

                <ol class="breadcrumb" style="margin-left: 295px " itemscope itemtype="http://schema.org/BreadcrumbList">
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                        <a href="{{url("/")}}" itemscope itemtype="http://schema.org/Thing" itemprop="item">
                            <span itemprop="name">Home</span>
                        </a>
                        <meta itemprop="position" content="1" />
                    </li>

                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                        <a  href="{{url("/shop/".$search["gender"]."/find")}}" itemscope itemtype="http://schema.org/Thing" itemprop="item" >
                            <span itemprop="name">{{$search["gender"]}}</span>
                        </a>
                        <meta itemprop="position" content="2" />
                    </li>
                </ol>

            </span>
            <br>
            <span style="padding-top: 20px !important; height: 200px !important; " class="" >
                <div class="row">
                <div class="col-md-3">
                    <h3 class="cat-h3">Filtern</h3>
                </div>

                <div class="col-md-9 ">

                    <h3 class="cat-h3" style="margin-left: 0px">{{reset($search["categories"])}}
                        <span class="cat-h3-count">( {{count($products)}} results) </span>
                    </h3>
                </div>
                    </div>
            </span>

        </div>
    </div>



    <div class="col-md-3" style="">


        <form action="" style="margin-top:20px !important;">
            @if(isset($search["brands"]))
                <div class="panel-group">
                    <div class="panel panel-default">
                        <h4 class="cat-h4">
                                Marken
                        </h4>

                        <div class="panel-body cat-filter-selectbox"  style="max-height: 200px;overflow-y:auto;">
                            <ul>
                            @foreach($search["brands"] as $brand)
                                <li>
                                    <label class="checkbox" >
                                        <input
                                                type="checkbox" {!! null !== old('brands') ?  in_array($brand, old('brands') ) ? 'checked' : null : null !!}
                                        name="brands[]" value="{{$brand}}"  />
                                        <i></i>
                                        {{$brand}}
                                        <small><a href="#"></a></small>
                                    </label>
                                </li>
                            @endforeach

                            </ul>
                        </div>
                    </div>
                </div>
            @endif

            @if(count($search["categories"]) > 1)
                <div class="panel-group" id="accordion-v2">
                    <div class="panel panel-default">
                            <h4>
                                 Kateghorie
                            </h4>
                        <div class="panel-body" >
                            <ul class="list-unstyled checkbox-list">
                                @foreach($search["categories"] as $category)
                                    <li>
                                        <label class="checkbox">
                                            <input {!! null !== old('categories') ?  in_array($category, old('categories') ) ? 'checked' : null : null !!}
                                                   type="checkbox"
                                                   name="categories[]" value="{{$category}}"  />
                                            <i></i>
                                            {{$category}}
                                            <small><a href="#"></a></small>
                                        </label>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            @endif

            @if(isset($search["maxprice"]) && $search["maxprice"] != 0)
                <div class="panel-group" id="accordion-v4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion-v4"
                                   href="#collapseFour">
                                    Preis
                                    <i class="fa fa-angle-down" id="range"></i>
                                </a>
                            </h2>
                        </div>
                        <div id="collapseFour" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <div class="slider-snap" id="slider-snap"></div>
                                <p class="slider-snap-text">
                                    <input type="number" step="0.01"
                                           class="slider-snap-value-lower"
                                           name="price[lower]" id="slider-snap-value-lower">
                                    <input type="number" step="0.01"
                                           class="slider-snap-value-lower"
                                           name="price[upper]" id="slider-snap-value-upper">
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            @endif

            @if(isset($search["colors"]) && count($search["colors"]) >= 1)
                <div class="panel-group" id="accordion-v5">
                    <div class="panel panel-default">
                        <h3>
                            Farbe
                        </h3>
                        <div id="collapseFive" class="panel-collapse collapse in">
                            <div class="panel-body" style="max-height: 200px;overflow-y: auto;">
                                <ul >
                                    @foreach($search["colors"] as $color)
                                        <li>
                                            <label class="checkbox">
                                                <input {!! null !== old('colors') ?  in_array($color, old('colors') ) ? 'checked' : null : null !!} type="checkbox" name="colors[]" value="{{$color}}"  />
                                                <i></i>
                                                {{$color}}
                                                <small><a href="#"></a></small>
                                            </label>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            <button type="submit" class="btn  btn-primary">Anwenden</button>
        </form>
    </div>



    <br>

    <div class="col-sm-9 cat-products-view-cfg">
        <ul class="list-inline clear-both">

            <li class="sort-list-btn" style="float: right">
                <label for="show">Sort by</label>
                <select id="sortby" class="form-control     " >
                    <option value="price desc">Price desc</option>
                    <option value="price asc">Price asc</option>
                    <option value="views desc">Popularity desc</option>
                    <option value="views asc">Popularity asc</option>
                </select>
            </li>



            <li class="sort-list-btn col-md-2" style="float: right">
                <label for="show">Products / page</label>
                <select id="show" class="form-control">
                    <option value="6">6</option>
                    <option value="12">12</option>
                    <option value="18">18</option>
                    <option value="all">30</option>
                </select>
            </li>
        </ul>
    </div>

    <br><br><br><br>

    <div class="col-md-9 cat-products-view" style="margin-bottom: 100px;">


        @foreach($products as $product)
        <div class="col-md-3" style="height: 450px;">
            <div class="item">
                <div class="thumbnail">
                    <img class="group list-group-image" src="{{$product->thumbnail}}_SL250_.jpg"
                         style="width: 200px;" alt="{{$product->name}}" />
                    <div class="caption">
                        <h4 class="group inner list-group-item-heading">
                            {{$product->name}}
                        </h4>

                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                <p class="lead">
                                    € {{number_format($product->price/100,2, ",", ".")}}
                                </p>
                            </div>

                            <div class="col-xs-12 col-md-6">
                                <a class="btn btn-primary viewbtn"
                                   href="{{url("shop/".$product->seo_slug."/".$product->id)}}">View</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
            <br>


    </div>
    <center>
        {!! $products->links() !!}
    </center>
    </div>



@endsection

@section('footer')

    <script src></script>
    <script>
        $(document).ready(function () {

            $(".showkind").on("click", function () {
                if(ajaxuse == true)
                {
                    return;
                }
                else
                {
                    ajaxuse = true;
                    $.ajax({
                        url: '{{url('visitor/edit/showkind/')}}' + "/" + $(this).attr("set"),
                        type: 'POST',
                        async: false,
                        dataType: "",
                        data: {
                            '_token': '{{csrf_token()}}'
                        },
                        success: function (result) {
                            location.reload();
                        }
                    });
                    ajaxuse = false;
                }
            });

            $("#cartadd").on("click", function () {
                if(ajaxuse == true)
                {
                    return;
                }
                else
                {
                    ajaxuse = true;
                    $.ajax({
                        url: '{{url('cart/add/')}}' + "/" + $(this).attr("asin") + "/" + $(this).attr("qty"),
                        type: 'POST',
                        async: false,
                        dataType: "",
                        data: {
                            '_token': '{{csrf_token()}}'
                        },
                        success: function (result) {
                            location.reload();
                        }
                    });
                    ajaxuse = false;
                }
            });
// Limit products/per page
            $("#show").on("change", function () {
                if(ajaxuse == true)
                {
                    return;
                }
                else
                {
                    ajaxuse = true;
                    $.ajax({
                        url: '{{url('visitor/edit/show/')}}' + "/" + $(this).val(),
                        type: 'POST',
                        async: false,
                        dataType: "",
                        data: {
                            '_token': '{{csrf_token()}}'
                        },
                        success: function (result) {
                            location.reload();
                        }
                    });
                   // return html;
                }
            });
// Sorting
            $('#sortby').on("change", function () {
                if(ajaxuse == true)
                {
                    return;
                }
                else
                {
                    ajaxuse = true;
                    $.ajax({
                        url: '{{url('visitor/edit/sort/')}}' + "/" + $(this).val(),
                        type: 'POST',
                        async: false,
                        dataType: "",
                        data: {
                            '_token': '{{csrf_token()}}'
                        },
                        success: function (result) {
                            location.reload();
                        }
                    });
                   // return html;
                }
            });

            ajaxuse = false;
            var range = document.getElementById('slider-snap');
            var rangeValues = [
                document.getElementById('slider-snap-value-lower'),
                document.getElementById('slider-snap-value-upper')
            ];


            noUiSlider.create(range, {
                start: [
                    {!! null !== old('price.lower') ?  old('price.lower') : (($search["minprice"]) / 100)  !!},
                    {!! null !== old('price.upper') ?  old('price.upper') : ( ($search["maxprice"]) / 100) !!}
                ],
                snap: false,
                connect: true,
                range: {
                    'min': {{($search["minprice"]) / 100}},
                    'max': {{($search["maxprice"]) / 100}}
                }
            });
            range.noUiSlider.on('update', function( values, handle ) {
                rangeValues[handle].value = values[handle];
            });


        });

    </script>

@endsection
