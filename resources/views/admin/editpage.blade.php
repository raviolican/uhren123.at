@extends('layouts.app')

@section('content')

    <!--=== End Breadcrumbs ===-->
    <div class="container content">
        @if(Session::has("notification"))
            <div class="alert alert-success fade in">
                {{Session::get("notification")}}
            </div>
        @endif

        @if (count($errors) > 0)
            @foreach ($errors->all() as $error)
                <div class="alert alert-danger fade in">
                    {{ $error }}
                </div>
            @endforeach
        @endif
        @include("layouts.adminNavigation")


        <div class="col-md-9">

            <form action="" method="post">
            {{csrf_field()}}
            <input type="hidden" name="_method". value="PATCH">
            <h1>Edit "{{$pPage->title}}" - # <span class="myPageId">{{$pPage->id}}</span></h1>
            <fieldset>
                <section>
                    <label for="label">Title</label>
                    <input type="text" class="form-control" name="title" value="{{$pPage->title}}">
                </section>

                <section>
                    <label for="label">SEO-Slug - <i>Enter a fancy URL here for your SEO strategy</i></label>
                    <input type="text" class="form-control" name="slug" value="NOT_IMPLEMENTED!">
                </section>

                <section>
                    <label for="label">Description</label>
                    <input type="text" class="form-control" name="description" value="{{$pPage->description}}">

                </section>

                <section>
                    <label for="label">HTML</label>
                    <textarea name="html" class="form-control" cols="80" rows="10" >{{$pPage->html}}</textarea>
                </section>
                <p>
                    Site is really blank if there is no HTML-Code - Its completely
                    up2you what it contains
                </p>

                <footer>
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <button type="button"  class="btn  btn-danger delete_site">Delete</button>
                    <a  href="{{url("/p/".$pPage->id."/IMPLEMENT")}}" class="btn ">Goto Page</a>
                </footer>
            </form>
        </div>
    </div>
@endsection


@section("footer")



    <script>
        $('document').ready(function() {
            $('.delete_site').click(function () {
                if(confirm("You sure you want to delete this page?\n This cannot be undone!")) {
                    $.ajax({
                        url: '{{url('/admin/pages/')}}'+ "/" + $(".myPageId").html(),
                        type: 'DELETE',
                        async: false,
                        data: {
                            '_token': '{{csrf_token()}}'
                        },
                        success: function(result) {
                            if(result == 1)
                                alert("Deleted!");
                            else
                                alert("Error while trying to delete page");
                        }
                    })
                }
            });
        });
    </script>
@stop