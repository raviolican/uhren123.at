<?php
/*
 * Todo: Implement EditCategory
 */
?>

@extends('layouts.app')

@section('content')
    <div class="containter">
        @if(Session::has("notification"))
            <div class="alert alert-success fade in">
                {{Session::get("notification")}}
            </div>
        @endif
        @if (count($errors) > 0)
            <div class="alert alert-danger fade in">
                Errors: <br>
                @foreach ($errors->all() as $error)
                    - {{ $error }} <br>
                @endforeach
            </div>
        @endif

        @include("layouts.adminNavigation")
            <div class="col-md-9">
                <div>
                    <fieldset>
                        <section>
                            @if(count($categories) > 0)
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Description</th>
                                        <th>Modify</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($categories  AS $cat)
                                        @if($cat->parent_id != 0)
                                            @continue
                                        @endif
                                        <tr>
                                            <td> {{$cat->id}}.0</td>
                                            <td> {{$cat->name}}</td>
                                            <td> {{$cat->description}}</td>
                                            <td>
                                                <button class="delete" value="{{$cat->id}}">Delete</button>
                                                <button class="edit" value="{{$cat->id}}">Edit</button>
                                            </td>
                                        @foreach($cat->children as $child)
                                            <tr style="margin-left: 50px;padding-left: 50px;">
                                                <td align="right" style="margin-left: 40px;"> {{$cat->id}}.{{$child->id}}</td>
                                                <td> {{$child->name}}</td>
                                                <td> {{$child->description}}</td>
                                                <td>
                                                    <button class="delete" value="{{$child->id}}">Delete</button>
                                                    <button class="edit" value="{{$child->id}}">Edit</button>
                                                </td>
                                            </tr>
                                            @endforeach
                                            </tr>
                                        @endforeach
                                    </tbody>

                                </table>
                            @else
                                <p>
                                    No categories to show. <br>
                                    However, the standart category is <i>Uncategorized</i>, where all
                                    products without a category will fall. <br> If you delete a category and the products has no
                                    other categories, it will automatically fall in to this category.
                                    <br>
                                </p>
                            @endif
                        </section>
                    </fieldset>
                </div>

                <br>

                <h2>New Cateogry</h2>
                <form action="" method="POST">
                    <input type="hidden" name="_method" value="PUT">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="g_analytics_code">Category Namet</label>
                        <input type="text" name="name" class="form-control" id="name" aria-describedby="nameHelp"/>
                        <small id="nameHelp" class="form-text text-muted"> </small>
                    </div>

                    <div class="form-group">
                        <label for="description">Description</label>
                        <input type="text" name="description"  class="form-control"
                               id="description" aria-describedby="nameHelp"/>
                        <small id="descriptionHelp" class="form-text text-muted"> </small>
                    </div>

                    <div class="form-group">
                        <label for="parent_id">Make Subcategory ~ <i>Max depth is 2</i></label>
                        <select name="parent_id" id="parent_id" class="form-control">
                            <option value="0">NONE (Create a Main Category)</option>
                            @foreach($categories as $cat)
                                @if($cat->parent_id == 0)
                                    <option value="{{$cat->id}}">{{$cat->name}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>

                    <footer>
                        <button type="submit" class="btn-u">Add</button>
                    </footer>
                </form>
            </div>

    </div>
@endsection

@section("footer")
    <script>
        $('document'). ready( function(){
            $ajaxInUse = false;
            $(".delete").on("click", function () {
                $item = $(this);
                if($ajaxInUse) {
                    return;
                } else {
                    $ajaxInUse = true;
                    $.ajax({
                        url: '{{url("admin/products/categories")}}' + "/" + $item.val(),
                        type: "DELETE",
                        data: {
                            '_token': '{{csrf_token()}}'
                        },
                        success: function(result) {
                            if(result['code'] == 1)
                                $item.parent().parent().remove();
                            else
                                alert(result['msg']);
                            $ajaxInUse = false;
                        },
                        error: function(result) {
                            alert("Error!");
                            $ajaxInUse = false;
                        }
                    });
                }
            });


            $(".sky-form").validate({
                rules: {
                    asin: {
                        required: true,
                        minlength: 10,
                        maxlength: 10

                    },
                    category: "required",
                    gender: "required"

                },
                submitHandler: function(form) {


                    // some other code
                    // maybe disabling submit button
                    // then:
                    $(form).submit();
                }
            });
        });
    </script>
@endsection