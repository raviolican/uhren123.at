@extends('layouts.app')
@section('footer')
    <script src="{{asset("js/codeeditor/codemirror.js")}}"></script>
    <link rel="stylesheet" href="{{asset("css/codeeditor/codemirror.css")}}">
    <script src="{{asset("js/codeeditor/mode/javascript/javascript.js")}}"></script>
    <script type="text/javascript">
        var area = document.getElementById('additional_js');
        var myCodeMirror = CodeMirror.fromTextArea(area, {
            lineNumbers: true
        });
    </script>
@endsection

@section('content')

    <div class="containter">
        @include("layouts.adminNavigation")

        <div class="col-md-5">
            @if (count($errors) > 0)
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger fade in">
                        {{ $error }}
                    </div>
                @endforeach
            @endif
            <h2> General Settings </h2>
                <p>
                    Modify general settings.
                </p>
            <form action="/admin/site/meta" method="POST">
                {{csrf_field()}}



                <div class="form-group">
                    <label for="inputTitle">Title</label>
                    <input type="text" name="title" value="{{$settings["title"]}}" class="form-control" id="inputTitle" aria-describedby="titleHelp">
                    <small id="titleHelp" class="form-text text-muted">Enter the name of this site.</small>
                </div>

                <div class="form-group">
                    <label for="inputDescription">Description</label>
                    <input type="text" name="description" value="{{$settings["description"]}}" class="form-control" id="inputDescription" aria-describedby="descriptionHelp" >
                    <small id="descriptionHelp" class="form-text text-muted">A short description.</small>
                </div>

                <div class="form-group">
                    <label for="inputKeywords">Keywords</label>
                    <input type="text" name="keywords" value="{{$settings["keywords"]}}" class="form-control" id="inputKeywords" aria-describedby="keywordsHelp" >
                    <small id="keywordsHelp" class="form-text text-muted">Comma seperated keywords (optional).</small>
                </div>

                <div class="form-group">
                    <label for="inputCopyright">Copyright</label>
                    <input type="text" name="copyright" value="{{$settings["copyright"]}}" class="form-control" id="inputCopyright" aria-describedby="copyrightHelp" >
                    <small id="copyrightHelp" class="form-text text-muted">Will appear in the footer.</small>
                </div>

                <div class="form-group">
                    <label for="inputRevisitAfter">Revisit-After</label>
                    <input type="text" name="revisit_after" value="{{$settings["revisit_after"]}}" class="form-control" id="inputRevisitAfter" aria-describedby="revisit_afterHelp" >
                    <small id="revisit_afterHelp" class="form-text text-muted">Revisit-After for searche ngines. </small>
                </div>


                <div class="form-group">
                    <label for="additional_js">Additional HTML inside <i>Header</i></label>
                    <textarea type="text" name="additional_js" value="{{$settings["revisit_after"]}}" class="form-control" id="additional_js" aria-describedby="additional_jsHelp" >{{$settings["additional_js"]}}</textarea>
                    <small id="additional_jsHelp" class="form-text text-muted">Will implement JS/CSS before closing /head tag.</small>
                </div>

                <section>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </section>

            </form>

        </div>

    </div>
@endsection