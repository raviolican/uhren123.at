@extends('layouts.app')
@section('header')
    <link rel="stylesheet" href="{{asset("css/owl-carousel/assets/owl.carousel.min.css")}}">

@endsection
@section('content')
    <ul class="bxslider" styx style="">

    @foreach($slides as $slide)
            <li style="height: 400px;"><img src="{{url($slide->image)}}"   title="{{$slide->caption}}" /></li>
    @endforeach

    </ul>
    <br>

    <br>
    <div class="container">
        @if(count($featuredProducts) > 0)
        <h3 style="margin: 0 auto !important;"><b>Featured Products</b></h3>
        <br>
        <div id="products" class="row list-group  ">
            <div class="owl-carousel">
                @foreach($featuredProducts as $feature)
                    <div class="item">
                        <div class="thumbnail">
                            <img class="group list-group-image" src="{{$feature->thumbnail}}_SL250_.jpg" style="width: 200px;" alt="{{$feature->name}}" />
                            <div class="caption">
                                <h4 class="group inner list-group-item-heading">
                                    {{$feature->name}}
                                </h4>

                                <div class="row">
                                    <div class="col-xs-12 col-md-6">
                                        <p class="lead">
                                            € {{number_format($feature->price/100,2, ",", ".")}}
                                        </p>
                                    </div>

                                    <div class="col-xs-12 col-md-6">
                                        <a class="btn btn-success" href="{{url("shop/".$feature->seo_slug."/".$feature->id)}}">View</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        @endif
        <h3 style="margin: 0 auto !important;">Latest Products</h3>
        <br>
        <div id="products" class="row list-group  " style="border: none !important;">
            <div class="owl-carousel">
                @foreach($latestProducts as $latest)
                    <div class="item" style="border: 0px solid black !important;">
                        <div class="thumbnail" style="border: 0px solid black !important;">
                            <img class="group list-group-image"
                                 src="{{$latest->thumbnail}}_SL250_.jpg" style="width: 200px;" alt="{{$latest->name}}" />
                            <div class="caption">

                            <h4 class="group inner list-group-item-heading">{{$latest->name}}</h4>

                            <div class="row">
                                <div class="col-xs-12 col-md-6">
                                    <p class="lead">
                                        € {{number_format($latest->price/100,2, ",", ".")}}
                                    </p>
                                </div>

                                <div class="col-xs-12 col-md-6">
                                    <a class="btn btn-success" href="{{url("shop/".$latest->seo_slug."/".$latest->id)}}">View</a>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script src="{{asset('/js/slider/jquery.bxslider.min.js')}}"></script>
    <!-- bxSlider CSS file -->
    <link href="{{asset('/css/slider/jquery.bxslider.css')}}" rel="stylesheet" />
    <script src="{{asset("js/owl-slider/owl.carousel.min.js")}}"></script>
    <script>
        $(document).ready(function () {
            $(document).ready(function(){
                $('.bxslider').bxSlider({
                    captions: true,

                });
                $(".owl-carousel").owlCarousel({

                    navigation: true, // Show next and prev buttons
                   // autoWidth:true,
                    slideSpeed: 300,
                    paginationSpeed: 400,
                    singleItem: false,
                    transitionStyle: "fadeUp",
                    autoPlay: true,
                });
            });
        });
    </script>

@endsection
