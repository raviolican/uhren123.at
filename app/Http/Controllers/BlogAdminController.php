<?php

namespace App\Http\Controllers;

use App\Article;
use Illuminate\Http\Request;

use App\Http\Requests;
use Mockery\CountValidator\Exception;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class BlogAdminController extends Controller
{
    /*
     *
     */
    public function viewBlogPOsts()
    {
        $articles = \App\Article::paginate(50);
        return view('admin.viewblogposts', compact('articles'));
    }
    public function newBlogPost(Request $request)
    {
        return view('admin.addblogposts');
    }
    public function savePost(Request $request)
    {
        $this->validate($request, [
            "image" => "required"
        ]);

        $path = $request->file("image")->store("article_images");




        $article = new Article();
        $article->title = $request->title;
        $article->seo_slug = $request->seo_slug;
        $article->category = $request->category;
        $article->text = $request->text;
        $article->description = $request->description;
        $article->keywords = $request->keywords;
        $article->article_image = $path;
        $article->image_organization = $request->image_organization;
        $article->image_credit = $request->image_credit;

        if($article->save())
            session()->flash("notification", "Article saved.");
        else
            $request->session()->flash("error", "Could not save article.");
        return redirect()->back();
    }

    /**
     * ~~ DEPRECATED ~~
     * @param $image
     * @return string
     */
    public function saveImage($image){

        $file = array('image' => $image);
        $rules = array('image' => 'required');
        $validator = \Validator::make($file, $rules);
        if ($validator->fails()) {
            // send back to the page with the input data and errors
            throw new FileException();
        }
        else {
            dd($image->isValid());
            if ($image->isValid()) {
                $destinationPath = 'uploads'; // upload path
                $extension = $image->getClientOriginalExtension(); // getting image extension
                $fileName = rand(11111,99999).'.'.$extension; // renameing image
                $image->move($destinationPath, $fileName); // uploading file to given path
                // sending back with message
                session()->flash('success', 'Upload successfully');
                return $fileName;
            }
            else {
                // sending back with error message.
                Session::flash('error', 'uploaded file is not valid');
                throw new FileException();
            }

        }
    }
    public function updatePostView(Request $request, $id)
    {
        $article = Article::where("id", $id)->get()->first();
        return view("admin.editBlogPost", compact("article"));
    }
    public function updatePost(Request $request, $id)
    {
        $file = FALSE;
        // If the article image is changed...
        if(isset($request->file)){
            $oldFile = Article::select("article_image")->where("id", $id)->get()->first();
            $newFile = $this->saveImapge();
            File:delete("uploads/".$oldFile->article_image);
            $file = true;
        }
        try{
            $re = $request->except("_token");
            if($file)
                $re["article_image"] = $newFile;
            $article = Article::where("id", $id)->update($re);
            $request->session()->flash("notification", "Article updated.");
            return redirect()->back();

        } catch (Exception $e) {
            return redirect()->back()->withErrors("Could not update article.");
        }


    }
}
