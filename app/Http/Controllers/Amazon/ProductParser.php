<?php

namespace App\Http\Controllers\Amazon;

use App\Product;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use League\Flysystem\Exception;

class ProductParser
{
    private $variation; /* (Amazon) */
    private $xml;
    public function __construct($product, $variations)
    {
        $this->variation = $variations;
        $this->xml   = $product;

    }

    /**
     * Parses the XML response from Amazon. Creates a new instance of type Product
     * returns it back to the caller.
     * @return $this|Product
     * @throws QueryException
     */
    public function parse()
    {
        $product = new Product();

        /*
        *   First things first
        */
        $product->name      = $this->xml->Items->Item->ItemAttributes->Title;
        $product->salesRank = $this->xml->Items->Item->SalesRank;
        $product->ItemAttributes = json_encode($this->xml->Items->Item->ItemAttributes);

        $product->features  = json_encode($this->xml->Items->Item->ItemAttributes->Feature);

        // Fancy Product Names
        $product->seo_slug = $this->seoUrl($this->xml->Items->Item->ItemAttributes->Title);

        $product->brand = $this->xml->Items->Item->ItemAttributes->Brand;
        $product->asin  = $this->xml->Items->Item->ASIN;

        if(isset($this->xml->Items->Item->EditorialReviews->EditorialReview->Content))
            $product->editorialreview = $this->xml->Items->Item->EditorialReviews->EditorialReview->Content;
        else
            $product->editorialreview = " ";


        try {
            if($this->variation)
            {
                // Get FIRST variation and assign it!
                $product->color = $this->xml->Items->Item->Variations->Item->ItemAttributes->Color;
                $product->ean =
                    $this->xml->Items->Item->Variations->Item->ItemAttributes->EAN;
                $product->price =
                    $this->xml->Items->Item->Variations->Item->Offers->Offer->OfferListing->Price->Amount;

                // Thumbnail Image
                $thumb = (string)$this->xml->Items->Item->SwatchImage->URL;
                $pos = strpos($thumb, "_");
                $trimedImg = substr($thumb,0,$pos );
                $thumb = str_replace("http://ecx.images-amazon.com/","https://images-eu.ssl-images-amazon.com/",$trimedImg);

                $product->thumbnail = $thumb;

                $images = $this->xml->Items->Item->Variations->Item->ImageSets->ImageSet;

                // Reference for the images to be save on the DB
                $dbImages = array();

                foreach($images AS $image)
                {
                    $image = $image->LargeImage->URL;
                    $pos = strpos($image, "_");
                    $timedImg = substr($image,$pos, -1 );
                    $fin =  str_replace("http://ecx.images-amazon.com/","https://images-eu.ssl-images-amazon.com/", $timedImg);
                    $dbImages[] = $fin;
                }

                $product->imgurls = implode(",", $dbImages);

            } else { /* No Variations */
                // No veriation ... easy going...
                $product->color = $this->xml->Items->Item->ItemAttributes->Color;
                $product->ean = $this->xml->Items->Item->ItemAttributes->EAN;
                $product->price = $this->xml->Items->Item->OfferSummary->LowestNewPrice->Amount;

                // Thumbnail!
                $thumb = (string)$this->xml->Items->Item->MediumImage->URL;
                $pos = strpos($thumb, "_");
                $trimedImg = substr($thumb,0,$pos );
                $thumb = str_replace("http://ecx.images-amazon.com/","https://images-eu.ssl-images-amazon.com/",$trimedImg);
                $product->thumbnail = $thumb;


                // Check if IMAGES available!
                $images = $this->xml->Items->Item->ImageSets->ImageSet;

                // Error
                if(count($images) <= 0)
                    return redirect()->back()->withErrors('Error while trying to fetch product images.');

                // Create URL to store the images
                $dbImages = array();

                // Looop through each image and make its HTTPS =)
                foreach($images AS $image)
                {
                    $image = (string)$image->SwatchImage->URL;
                    $pos = strpos($image, "_");
                    $trimedImg = substr($image,0,$pos );
                    $fin =  str_replace("http://ecx.images-amazon.com/","https://images-eu.ssl-images-amazon.com/",$trimedImg);
                    $dbImages[] = $fin;
                }

                $product->imgurls = implode(",", $dbImages);
                // TODO: Implement this... maybe
                #$product->amountsaved =
                #   $amazon->Items->Item->Offers->Offer->OfferListing->AmountSaved->Amount;
            }
        } catch (QueryException $e) {
            throw $e;
        }

        /* Return the play product */
        return $product;

    }

    public function hadErrors()
    {

    }

    /**
     * @param $string
     * @return mixed|string
     */
    private function seoUrl($string) {

        # NOT_IMPLEMENTED $string = removeCommonWords($string);
        $string = str_replace( array ("ä", "ö", "ü", "ß"), array ("ae", "oe", "ue", "ss"),$string);
        $string = trim($string); // Trim String
        $string = strtolower($string); //Unwanted:  {UPPERCASE} ; / ? : @ & = + $ , . ! ~ * ' ( )
        $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);  //Strip any unwanted characters
        $string = preg_replace("/[\s-]+/", " ", $string); // Clean multiple dashes or whitespaces
        $string = preg_replace("/[\s_]/", "-", $string); //Convert whitespaces and underscore to dash
        return $string;

    }
}
