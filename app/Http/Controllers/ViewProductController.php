<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Amazon\Amazon;
use App\Product;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Http\Requests;
use Mockery\CountValidator\Exception;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
class ViewProductController extends Controller
{
    public function renderProductPage(Request $request,$name, $id)
    {
        $product =  Product::find($id);

        if($product == NULL)
            throw new NotFoundHttpException();

        $product->increment("views");

        // Doing some session stuff ...
        SessionController::viewedProducts($request,$product->asin);

        // Done
        $amazon = json_decode(\Cache::remember("amzProduct_".$id, 20, function() use ($product) {
            $amazon = new Amazon();
            return \GuzzleHttp\json_encode($amazon->searchProductByASIN($product->asin,array('OfferFull' ) ));
            // return json_encode(ShoppingController::amazonLookup($product->asin, array('Variations', 'OfferFull' )));
        }));

        if($amazon === FALSE)
            return redirect()->back()->withErrors('Error while trying to lookup the product. Is the ASIN correct?');
        if(isset($amazon->Items->Item->Variations) )
            $hasVariations = ShoppingController::validateVariations($amazon->Items->Item->Variations);
        else
            $hasVariations = FALSE;
        //dd($amazon);
        // Creating the first elements
        $productArray = array(
            "asin" => $product->asin,
            "name" => $product->name,
            "brand" => $product->brand,
            "thumbnail" =>  $product->thumbnail,
            "main_category" => $product->categories,
            "imgurls" => explode(',',$product->imgurls),
            "editorreview" => $product->editorialreview,
            "attributes" => json_decode($product->ItemAttributes ),
            "thumbnail" => $product->thumbnail,
            "features" => json_decode( \Cache::remember($id, 0, function() use ($id) {
                #  Product::where('id', $productId)->select("features")->get()[0]
                return (Product::where('id', $id)
                    ->select("features")->get()->first()["features"]);
            }))
        );

        if($hasVariations) {
            ShoppingController::updatePrice($product, $amazon->Items->Item->Variations->Item->Offers->Offer->OfferListing->Price->Amount);
            $productArray['price'] = $amazon->Items->Item->Variations->Item->Offers->Offer->OfferListing->Price->Amount;
        } else {
            //$amazon = new Amazon();

            // old ShoppingController::updatePrice($product, $amazon->Items->Item->OfferSummary->LowestNewPrice->Amount);

            $pr = $amazon->Items->Item->OfferSummary->LowestNewPrice->Amount;

            if($product->price != $pr) {
                $product->price = $pr;
                $product->increment("price_updates");
                $product->save();
            }

            try {
                $productArray['price'] = $amazon->Items->Item->OfferSummary->LowestNewPrice->Amount;
            } catch (Exception $e){
                return redirect()->Back()->withErrors("Das Produkt ist im Moment leider nicht verfügbar.");
            }
        }

        $recommendations = ShoppingController::getLatestViewedProducts();
        $sameCatProducts = $this->getSameCatProducts($product);

        $recommendations->merge($sameCatProducts)->unique();

        $meta = $this->compileMeta($productArray);
        return view("viewproduct", compact('productArray', 'recommendations', 'meta'));
    }

    /**
     * @param $productArray
     * @return array
     */
    private function compileMeta($productArray){
        $meta = array();
        $t_len = strlen($productArray["name"]);
        $d_len = 166;
        $c = strlen($productArray["editorreview"]);
        $add = $c/$d_len;
        if(count($productArray["editorreview"]) > 0) {
            if($add < 1) {
                $k = 1 - $add;
                $num = $k * $d_len;
                if($t_len >= $num)
                    $meta["description"] = strip_tags($productArray["editorreview"])." ".substr($productArray["name"],0,$num);
                else
                    $meta["description"] = " bei uhren123. Hochwertige "
                        .$productArray["brand"]." günstig.";
            } else {
                $meta["description"] = substr(strip_tags($productArray["editorreview"]),0,157)."..";
            }
        } else {
            if($add < 1) {
                $k = 1 - $add;
                $num = $k * $d_len;
                if($t_len >= $num)
                    $meta["description"] = strip_tags($productArray["editorreview"])." ".substr($productArray["name"],0,$num);
                else
                    $meta["description"] = ucfirst($productArray["gender"])." bei uhren123. Hochwertige "
                        .$productArray["brand"]." ".$productArray["category"]. " günstig.";
            } else {
                $meta["description"] = substr(strip_tags($productArray["editorreview"]),0,157)."..";
            }
        }
        $meta["title"] = $productArray["name"];
        $meta["keywords"] = ", ".$productArray["brand"].", uhren123";
        return $meta;
    }

    private function getSameCatProducts($products)
    {

        $cat = $products->categories;
        $products = new Collection();
        foreach ($cat as $item) {
            $products->push(
                $item->products->random(1)->first());
            //var_dump($item->products);
           // $products+=$item->products;

        }

        /*
        $products = \DB::table("products")->select("name","color","id", "price",
            "thumbnail", "seo_slug")
            ->where(function ($query) use ($cat) {
               foreach ($cat AS $c)
               {
                   $query->orWhere()
               }
            });
        */
        return $products;
    }
}
