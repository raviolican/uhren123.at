<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'ShoppingController@index');

Route::get('/slides/{filename}', function ($filename)
{
    $path = storage_path() . '\\app\\slides\\' . $filename;
    // dd($path);
    if(!File::exists($path)) abort(404);

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response;
});

Route::get('/article_images/{filename}', function ($filename)
{
    $path = storage_path() . '\\app\\article_images\\' . $filename;
    // dd($path);
    if(!File::exists($path)) abort(404);

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response;
});



/*
 * ADMINISTRATOR ROUTING
 * This provides routing for administration sections and
 * functionallity. Watch out that middleware is auth and also admin.
 */
Route::group(['middleware' => 'auth'], function () {
    Route::group(['middleware' => 'admin'], function () {
        Route::group(['prefix' => 'admin'], function () {

            // DASHBOARD
            Route::get('', function () {
                return view('admin.dashboard');
            });

            Route::group(['prefix' => 'site'], function () {
                Route::group(['prefix' => 'meta'], function () {
                    Route::get('', function ()    {
                        return view('admin.editmeta');
                    });
                    Route::post('', "AdminController@patchSiteSettings");
                });
                Route::group(['prefix' => 'general'], function () {
                    Route::get('', function ()    {
                        return view('admin.general');
                    });
                    Route::patch('', "AdminController@patchSiteSettings");
                });
            });

            Route::group(['prefix' => 'google'], function () {
                Route::get('', function ()    {
                    return view('admin.googleSettings');
                });
                Route::post('', "AdminController@patchSiteSettings");

            });

            Route::group(['prefix' => 'slides'], function () {
                Route::get('', "adminController@renderSlidesPage");
                Route::put('', "AdminController@putSlider");
                Route::patch('', "AdminController@patchSlider");
                Route::delete('', "AdminController@deleteSlider");

            });

            Route::group(['prefix' => 'amazon'], function () {
                Route::get('', function ()    {
                    return view('admin.amazonSettings');
                });
                Route::post('', "AdminController@patchSiteSettings");

            });

            Route::group(['prefix' => 'products'], function () {
                Route::group(['prefix' => 'add'], function () {
                    Route::get('', function () {
                        $fetchCats = \App\Category::all();
                        return view('admin.addproduct', compact('fetchCats'));
                    });
                    Route::POST("", "AdminController@storeNewProduct")->name('addNewProduct');
                });

                Route::group(['prefix' => 'categories'], function () {
                    Route::put("", "AdminController@addCategory");
                    Route::get('', function ()    {
                        $categories = \App\Category::all();
                        return view('admin.categories', compact('categories'));
                    });

                    Route::delete("{id}", "AdminController@deleteCategory");



                    # Route::PUT("", "AdminController@saveNewProduct");
                });

                // Delete a Product!
                Route::post("/delete/{id}", "AdminController@removeProduct");

                Route::post("feature/{id}/{status}", function ($id, $status){
                    \App\Product::where("id", $id)->update(["featured"=> $status]);
                    \Illuminate\Support\Facades\Cache::forget("featured");
                    return $status;
                });

                Route::get('/view', 'AdminController@viewProducts');
            });

            Route::group(["prefix"=>"blog"], function (){
                Route::get("/view", "BlogAdminController@viewBlogPOsts");
                Route::get("/add", "BlogAdminController@newBlogPost");
                Route::post("/add", "BlogAdminController@savePost");
                Route::post("/update/{id}", "BlogAdminController@updatePost");
                Route::get("/update/{id}", "BlogAdminController@updatePostView");
            });

            Route::group(['prefix' => 'pages'], function () {
                Route::get('new', function() {
                    return view('admin.addpage');
                });
                Route::post('new', 'AdminController@addPage');

                Route::get('{id}', function ($id)    {
                    $pPage = \App\Site::find($id);
                    if($pPage == NULL)
                        throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException();
                    return view('admin.editpage', compact('pPage'));
                });

                Route::patch('{id}', "AdminController@updatePage");
                Route::DELETE('{id}', function($id) {
                    return (string)\App\Site::find($id)->delete();
                });
            });
        });
    });
});

// Custom Pages
Route::group(['prefix' => 'p'], function () { // post
    Route::get('/{id}/{slug}','ShoppingController@renderPage');
});


Route::group(['prefix' => 'visitor'], function ($prefix) { // post

    Route::group(['prefix' => 'edit'], function ($prefix) { // post
        Route::post("/show/{number}", "ShoppingController@editShowNumber");
        Route::post("/sort/{kind}", "ShoppingController@editSortBy");
        Route::post("/showkind/{kind}", function ($kind){
            if(in_array($kind, ["tile", "list"])){
                Request()->session()->put("showkind", $kind );
            } else {
                return false;
            }

        });
    });

    /**
     * Cart functionallity
     */

});

Route::group(['prefix' => 'blog'], function ($prefix) { // post
    Route::get("/", "BlogController@index");
    Route::get("/view/{id}/{slug}", "BlogController@viewArticle");
});




Route::group(['prefix' => 'cart'], function ($prefix) { // post
    Route::post("add/{asin}/{qty}", "ShoppingCartController@addProductToCart");
    Route::post("delete/{asin}", "ShoppingCartController@deleteProductFromCart");
});
Route::get("cart", "ShoppingCartController@renderShoppingCart");

Route::group(['prefix' => 'shop'], function ($prefix) { // post

    Route::get('{gender}/find/{categories?}/{brands?}/{price?}/{colors?}',
        'ShoppingController@renderCategoryPage');
    #old Route::get('{gender}/v/{category}/{id}/{name}', 'ViewProductController@renderProductPage');
    Route::get('{name}/{id}/', 'ViewProductController@renderProductPage');

});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

